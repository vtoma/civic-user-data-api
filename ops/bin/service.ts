#!/usr/bin/env node
import * as cdk from '@aws-cdk/core';
import 'source-map-support/register';
import {CodePipelineStack} from '../lib/code-pipeline-stack';
import {ServiceStack} from '../lib/service-stack';

let account = '835251680420';
const env = {account: account, region: 'eu-west-1'};

const app = new cdk.App();

const serviceStackDev = new ServiceStack(app, 'stackDevelopCiv', {
    stackPrefix: 'developciv',
    env
});

new CodePipelineStack(app, 'pipelineDevelopCiv', {
    stackPrefix: serviceStackDev.prefix,
    serviceStackName: serviceStackDev.stackName,
    codeCommitRepositoryArn: `arn:aws:codecommit:eu-west-1:${account}:decision-step`,
    branchName: 'develop',
    buildSpecFilename: 'spec/buildspec-dev.yml',
    env
});