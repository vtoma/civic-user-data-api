import {Construct, Duration, RemovalPolicy} from '@aws-cdk/core';
import {ServiceStackProps} from './stack-props';
import {join} from "path";
import {RetentionDays} from '@aws-cdk/aws-logs';
import {KinesisEventSource} from '@aws-cdk/aws-lambda-event-sources';
import {Function, Runtime, Code, Tracing, StartingPosition} from '@aws-cdk/aws-lambda';
import {Stream, IStream} from '@aws-cdk/aws-kinesis';
import {Policy, IRole, PolicyStatement, Role, ServicePrincipal, Effect} from '@aws-cdk/aws-iam';
import {AttributeType, Table} from '@aws-cdk/aws-dynamodb';
import {lambdaPolicy} from './lambda-policy'

export class GeneralStack {
    protected prefix: string;
    protected stack: Construct;
    protected lambdaPolicy: Policy;
    protected lambdaRole: IRole;

    constructor(stack: Construct, props: ServiceStackProps, prefix: string) {
        this.prefix = prefix;
        this.stack = stack;
        // this.createLambdaPolicy();
    }

    protected createLambda(name: string, path: string, stream?: Stream, env: object = {}, timeout: number = 3): Function {
        const fn = new Function(this.stack, this.prefix + name, {
            functionName: this.prefix + name,
            code: Code.fromAsset(join(__dirname, path)),
            runtime: Runtime.GO_1_X,
            handler: 'main',
            tracing: Tracing.ACTIVE,
            timeout: Duration.seconds(timeout),
            environment: {
                PREFIX: this.prefix,
                ...env
            },
            logRetention: RetentionDays.ONE_DAY,
        });

        if(stream) {
            const eventSource = new KinesisEventSource(stream, {
                startingPosition: StartingPosition.TRIM_HORIZON,
            });
            fn.addEventSource(eventSource);
        }

        // this.lambdaPolicy.attachToRole(fn.role as IRole);

        return fn;
    }

    protected createKinesisStream(streamName: string): Stream {
        const stream = new Stream(this.stack, `${this.prefix}${streamName}`, {
            streamName: `${this.prefix}${streamName}`,
            retentionPeriod: Duration.hours(48),
        });

        return stream;
    }

    protected createDynamoTable(tableName: string, kinesisStream?: IStream): Table {
        const partitionKey = {name: "uuid", type: AttributeType.STRING}
        const table = new Table(this.stack, `${this.prefix}-${tableName}`, {
            tableName: `${this.prefix}-${tableName}`,
            partitionKey,
            kinesisStream: kinesisStream ?? undefined,
            removalPolicy: this.prefix == "prod" ? undefined : RemovalPolicy.DESTROY
        });

        return table;
    }

    protected createLambdaPolicy() {
        this.lambdaPolicy = new Policy(this.stack, `${this.prefix}BuildLambdaPolicy`, {
            policyName: `${this.prefix}BuildLambdaPolicy`,
            statements: [PolicyStatement.fromJson(lambdaPolicy)]
        });

        this.lambdaRole = new Role(this.stack, `${this.prefix}-LambdaRole`, {
            roleName: `${this.prefix}-LambdaRole`,
            assumedBy: new ServicePrincipal('lambda.amazonaws.com')
        });

        this.lambdaRole.addToPrincipalPolicy(new PolicyStatement({
            effect: Effect.ALLOW,
            actions: [
                'kinesis:*',
                'logs:*'
            ],
            resources: [`*`],
        }));
    }
}