cd ../ && GOOS=linux go build -o main \
&& echo "Creating ZIP..." \
&& zip function.zip main \
&& echo "DONE" \
&& echo "Uploading to AWS..." \
&& aws lambda update-function-code --function-name  developCreateUserLambda --zip-file fileb://function.zip --profile payingit >> /dev/null \
&& echo "DONE" \
&& rm main \
&& rm function.zip
