# User Data API (Civic test)

## Prerequisites:
make sure you have installed on your system these tools
- [aws cli](https://aws.amazon.com/cli/)
- [aws cdk](https://docs.aws.amazon.com/cdk/v2/guide/cli.html)

## Configure

Install dependencies:
```
cd ./ops
npm install
```

Set you AWS account id in `ops/bin/service.ts`:
```
let account = '835251680420';
```

If you would like to use the Pipeline you'll have to also configure your repo in: `ops/bin/service.ts:20`

## Deploy
```
cd ./ops
cdk synth
cdk deploy stackDevelopCiv --profile [your profile]
```
## Architecture docs
Architecture overview can be found in `architecture/README.md`