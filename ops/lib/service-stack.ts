import {Construct, Stack} from '@aws-cdk/core';
import {ServiceStackProps} from './stack-props';
import {DecisionStack} from './decision-substack';
import {UserDataStack} from './user-data-substack';

export class ServiceStack extends Stack {
    public prefix: string;

    constructor(scope: Construct, id: string, props: ServiceStackProps) {
        super(scope, id, props);

        // this order is important
        new DecisionStack(this, props).generate();
        new UserDataStack(this, props).generate();
    }

    

}
