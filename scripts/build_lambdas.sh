#!/bin/bash

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

build_functions () {
    echo "Building lambdas..."
    cd ../src/lambda/
    find . -name main.go -print0 | xargs -0 -n1 dirname | sort --unique | while read dirname; do
        pushd "$dirname";
        echo "building go for $dirname..."
        GOOS=linux go build -o main .
        status=$?
        [ $status -eq 0 ] || exit $status
        popd;
    done
}

build_functions