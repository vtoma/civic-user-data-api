module civ

go 1.16

require (
	github.com/aws/aws-lambda-go v1.27.0
	github.com/aws/aws-sdk-go v1.42.25
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/gofiber/template v1.6.25 // indirect
	github.com/google/uuid v1.3.0
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/opensearch-project/opensearch-go v1.0.0
	github.com/sirupsen/logrus v1.8.1
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
)
