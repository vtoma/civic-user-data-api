import {ComputeType, IProject, LinuxBuildImage, PipelineProject, Project, Source, Cache} from '@aws-cdk/aws-codebuild';
import {BuildSpec} from '@aws-cdk/aws-codebuild/lib/build-spec';
import {Repository} from '@aws-cdk/aws-codecommit';
import {IRepository} from '@aws-cdk/aws-codecommit/lib/repository';
import {Artifact, Pipeline} from '@aws-cdk/aws-codepipeline';
import {Action, CodeBuildAction, CodeCommitSourceAction} from '@aws-cdk/aws-codepipeline-actions';
import {LogGroup, RetentionDays} from '@aws-cdk/aws-logs';
import {Construct, Stack, StackProps, RemovalPolicy} from '@aws-cdk/core';
import {Bucket, BucketAccessControl} from '@aws-cdk/aws-s3';
import * as iam from '@aws-cdk/aws-iam';

export interface CodePipelineProps extends StackProps {
    readonly stackPrefix: string;
    readonly branchName: string;
    readonly buildSpecFilename: string;
    readonly codeCommitRepositoryArn: string;
    readonly serviceStackName: string;
}

export class CodePipelineStack extends Stack {
    private readonly prefix: string;
    private readonly repository: IRepository;
    private codeBuildProject: IProject;
    private sourceAction: Action;
    private buildAction: Action;
    private serviceStackName: string;
    private stack: Stack;

    constructor(scope: Construct, id: string, private props: CodePipelineProps) {
        super(scope, id, props);

        this.prefix = this.props.stackPrefix;
        this.serviceStackName = this.props.serviceStackName;
        this.repository = Repository.fromRepositoryArn(this, `${this.prefix}-repository`, this.props.codeCommitRepositoryArn);

        this.createCodeBuildProject();
        this.definePipelineActions();
        this.createCodePipeline();
    }

    private createCodeBuildProject(): void {
        const pipelineCache = new Bucket(this, `${this.prefix}-cache-bucket`, {
            bucketName: `${this.prefix}-cache-bucket`,
            removalPolicy: RemovalPolicy.DESTROY,
            autoDeleteObjects: true,
            publicReadAccess: false,
            accessControl: BucketAccessControl.PRIVATE
        });

        this.codeBuildProject = new Project(this, `${this.prefix}-codebuild`, {
            source: Source.codeCommit({repository: this.repository, branchOrRef: this.props.branchName}),
            buildSpec: BuildSpec.fromSourceFilename(this.props.buildSpecFilename),
            environment: {
                buildImage: LinuxBuildImage.fromCodeBuildImageId('aws/codebuild/standard:5.0'),
                computeType: ComputeType.MEDIUM,
                environmentVariables: {
                    "STACK_NAME": {value: this.serviceStackName}
                },
                privileged: true,
            },
            logging: {
                cloudWatch: {
                    logGroup: new LogGroup(this, `${this.prefix}-codebuild-log-group`, {
                        retention: RetentionDays.TWO_MONTHS
                    })
                }
            },
            grantReportGroupPermissions: true,
            cache: Cache.bucket(pipelineCache),
        });

        this.codeBuildProject.addToRolePolicy(new iam.PolicyStatement({
            effect: iam.Effect.ALLOW,
            actions: [
                'ssm:*',
                'cloudformation:*',
                'dynamodb:*',
                'iam:*',
                's3:*',
                'apigateway:*',
                'appsync:*',
                'sns:*',
                'sqs:*',
                'lambda:*',
                'logs:*',
                'cloudwatch:*',
                'ec2:*',
                'codebuild:*',
                'cloudsearch:*',
                'kinesis:*',
                'es:*',
                'cognito-idp:*',
                'cognito-identity:*',
            ],
            resources: ['*'],
        }));
    }

    private definePipelineActions(): void {
        const sourceOutput = new Artifact(`${this.prefix}-artifact-source-output`);
        const buildOutput = new Artifact(`${this.prefix}-artifact-build-output`);
        this.sourceAction = new CodeCommitSourceAction({
            actionName: `${this.prefix}-Source`,
            repository: this.repository,
            branch: this.props.branchName,
            output: sourceOutput
        });

        this.buildAction = new CodeBuildAction({
            actionName: `${this.prefix}-CodeBuild`,
            project: this.codeBuildProject,
            input: sourceOutput, // The build action must use the CodeCommitSourceAction output as input.
            outputs: [buildOutput], // optional
        });

    }

    private createCodePipeline(): void {
        const pipeline = new Pipeline(this, `${this.prefix}-pipline-project`, {
            pipelineName: `${this.prefix}-pipeline`,
            stages: [
                {
                    stageName: 'Source',
                    actions: [this.sourceAction]
                },
                {
                    stageName: 'Build',
                    actions: [this.buildAction]
                },
            ],
            artifactBucket: new Bucket(this, `${this.prefix}-artifact-bucket-s3`, {
                bucketName: `${this.prefix}-artifacts-s3`,
                removalPolicy: RemovalPolicy.DESTROY,
                autoDeleteObjects: true,
            })
        });
    }
}
