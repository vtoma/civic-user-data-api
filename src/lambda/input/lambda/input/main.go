package main

import (
	"context"
	"encoding/json"
	"os"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	log "github.com/sirupsen/logrus"
)

var (
	sess *session.Session
)

type Request map[string]interface{}

type RequestJson struct {
	Email string `json:"email"`
}

type Response struct {
}

func handleRequest(ctx context.Context, request Request) (Response, error) {
	log.Info("request: ", request["body"])

	msg := request["body"].(string)
	var msgJson RequestJson
	err := json.Unmarshal([]byte(msg), &msgJson)

	if err != nil {
		return Response{}, err
	}

	log.Info("json: ", msgJson)

	// validate Email

	response := Response{}

	svc := sns.New(sess)
	result, err := svc.Publish(&sns.PublishInput{
		Message:  aws.String(msg),
		TopicArn: aws.String(os.Getenv("SNS_ARN")),
	})

	if err != nil {
		log.Error("SNS error:", err)
		return response, err
	}

	log.Info("msg id:", *result.MessageId)

	return response, nil
}

func init() {
	if logLevel, err := log.ParseLevel(os.Getenv("LOG_LEVEL")); err == nil {
		log.SetLevel(logLevel)
	}

	sess, _ = session.NewSession(&aws.Config{
		Region: aws.String(os.Getenv("AWS_REGION"))},
	)
}

func main() {
	lambda.Start(handleRequest)
}
