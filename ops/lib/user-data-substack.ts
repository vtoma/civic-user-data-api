import {Construct} from '@aws-cdk/core';
import {GeneralStack} from './general';
import {ServiceStackProps} from './stack-props';
import {LambdaIntegration, RestApi, MethodLoggingLevel} from "@aws-cdk/aws-apigateway";
import {Function} from '@aws-cdk/aws-lambda';
import {Table} from '@aws-cdk/aws-dynamodb';
import {Topic} from '@aws-cdk/aws-sns';
import {Queue} from '@aws-cdk/aws-sqs';
import {SqsSubscription} from '@aws-cdk/aws-sns-subscriptions';
import {SqsEventSource} from '@aws-cdk/aws-lambda-event-sources';

export class UserDataStack extends GeneralStack {
    protected validationLambda: Function;
    protected storeLambda: Function;
    protected analyticsLambda: Function;
    protected sns: Topic;
    protected snsAnalytics: Topic;
    protected mainQueue: Queue;
    protected analyticsQueue: Queue;

    constructor(stack: Construct, props: ServiceStackProps) {
        super(stack, props, props.stackPrefix);
        this.stack = stack;
        this.prefix = props.stackPrefix;
        return this;
    }

    generate() {
        this.createMainSns();
        this.createAnalyticsSns();
        this.createValidationLambda();
        this.createApiGateway();
        this.createMainSqs();
        this.createAnalyticsSqs();
        this.createStoreLambda();
        this.createAnalyticsLambda();
    }

    protected createMainSqs() {
        const dql = new Queue(this.stack, `${this.prefix}MainDLQ`);
        this.mainQueue = new Queue(this.stack, `${this.prefix}MainQueue`, {
            queueName: `${this.prefix}MainQueue`,
            deadLetterQueue: {
                queue: dql,
                maxReceiveCount: 3
            }
        });

        this.sns.addSubscription(new SqsSubscription(this.mainQueue));
    }

    protected createAnalyticsSqs() {
        const dql = new Queue(this.stack, `${this.prefix}AnalyticsDLQ`);
        this.analyticsQueue = new Queue(this.stack, `${this.prefix}AnalyticsQueue`, {
            queueName: `${this.prefix}AnalyticsQueue`,
            deadLetterQueue: {
                queue: dql,
                maxReceiveCount: 3
            }
        });

        this.snsAnalytics.addSubscription(new SqsSubscription(this.analyticsQueue));
    }

    protected createMainSns() {
        this.sns = new Topic(this.stack, `${this.prefix}UserDataTopic`, {
            topicName: `${this.prefix}UserDataTopic`
        });
    }

    protected createAnalyticsSns() {
        this.snsAnalytics = new Topic(this.stack, `${this.prefix}UserDataAnalyticsTopic`, {
            topicName: `${this.prefix}UserDataAnalyticsTopic`
        });
    }

    protected createValidationLambda() {
        this.validationLambda = this.createLambda(`${this.prefix}ValidationLambda`, '../../src/lambda/input/lambda/input', undefined, {
            SNS_ARN: this.sns.topicArn,
        });
        this.sns.grantPublish(this.validationLambda);
        Table.fromTableName(this.stack, "1", this.prefix + "loSecDecision").grantReadData(this.validationLambda);
    }

    protected createStoreLambda() {
        this.storeLambda = this.createLambda(`${this.prefix}UserDataStoreLambda`, '../../src/lambda/input/lambda/input', undefined, {
            SNS_ARN: this.snsAnalytics.topicArn,
        });

        this.snsAnalytics.grantPublish(this.storeLambda);
        Table.fromTableName(this.stack, "2", this.prefix + "loSecDecision").grantReadData(this.storeLambda);
        Table.fromTableName(this.stack, "3", this.prefix + "hiSecUserData").grantWriteData(this.storeLambda);
    }

    protected createAnalyticsLambda() {
        this.analyticsLambda = this.createLambda(`${this.prefix}UserDataAnalyticsLambda`, '../../src/lambda/input/lambda/input', undefined, {});
        this.analyticsLambda.addEventSource(new SqsEventSource(this.analyticsQueue));
    }

    protected createApiGateway() {
        const apiId = "user-data-api-" + this.prefix;
        const decisionApi = new RestApi(this.stack, apiId, {
            restApiName: apiId,
            description: "API user data.",
            deployOptions: {
                tracingEnabled: true,
                loggingLevel: MethodLoggingLevel.INFO
            },
            cloudWatchRole: true
        });

        const decisionPlanId = "userDataApiUsagePlan" + this.prefix;
        const decisionUsagePlan = decisionApi.addUsagePlan(decisionPlanId, {});

        const lambdaIntegration = new LambdaIntegration(this.validationLambda, {});

        const apiResource = decisionApi.root.addResource("store-data")

        apiResource.addMethod("POST", lambdaIntegration, {apiKeyRequired: false});

        decisionUsagePlan.addApiStage({stage: decisionApi.deploymentStage});
    }
}