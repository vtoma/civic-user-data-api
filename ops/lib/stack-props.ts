import {StackProps} from '@aws-cdk/core';

export interface ServiceStackProps extends StackProps {
    readonly stackPrefix: string;
}