# Structure
There are 2 stacks represented:
**"Decision API"** - the API that receives initial data from customer and takes the decision wheather he is eligeble for second step.
**"User Data API"** - stores user data if the first step accepted the user.

## Decision API description

![Decision API](https://i.ibb.co/Bghq9cH/Screen-Shot-2022-03-20-at-10-17-06.png)

Decision API represents the data flow of the decision request required to confirm user’s age and decides if it’s allowed to use the system or not.

**API Gateway** - entry point of the request. This is the public endpoint accessible by the user, where it can submit its data.

**Input Lambda** - the lambda resolver attached to the API Gateway endpoint which handles the incoming request. It is going to trigger Notification event to pass the data further to other services. This kind of integration  with the SNS makes the whole service asynchronous, so the user is not blocked at this step waiting for the response/approval. This lambda resolver performs some basic validation of incomming data and uses **loSecDecision** (1) table to check if a decision for the current user is already present in the database. 

**SNS Dec** - the service which is receiving the data from the API Gateway lambda resolver and streams the data to a queued service where the event/data is going to be retrieved by other services which are meant to handle it.
Any interested service can subscribe to this topic.

**SQS Dec** - events queue, where the events are stored until some service is picking it up to handle it. Queue is needed no to lose the events which are coming from SNS.

**Decision Lambda** - contains the business logic which decides if the user is 18+ years old or not. If the requirements are met, it send an email/message to the user with the link where the user can submit its private data. Also, the decision is stored in the database (e).

**SES** - email service which sends an email to the user.

**loSecDecision** - low security storage where the decisions are stored.

**Kinesis** - the streaming service synchronised between decisions database and analytics service. It’s used to trigger an event when a CRUD operation is performed on **loSecDecision** database. This way a new analytics record can be created automatically when a new decision is added into database.

**Analytics Lambda** - lambda function which is triggered by the Kinesis stream and received the data which should be stored within analytics database.

**Redshift** - analytics data storage. Place where all analytics data is located.

## User Data API

![Decision API](https://i.ibb.co/zJzrPbv/Screen-Shot-2022-03-20-at-10-26-20.png)

User data flow architecture describes the flow of the user’s request when it’s trying to submit the private data within application.

**API Gateway** - entry point of the request. This is the public endpoint accessible by the user, where it can submit its private data.

**Validation Lambda** - the lambda resolver attached to the API Gateway endpoint which handles the incoming request. It is going to trigger Notification event to pass the data further to other services. This kind of integration  with the SNS makes the whole service asynchronous, so the user is not blocked at this step waiting for the response/approval. 
In case there is no such decision in the loSecDecision lambda will respond back instantly tot he user with an appropriate message. For this reason, the connection between this resolver and **loSecDecision** (1) is required.

**SNS** - the service which is receiving the data from the API Gateway lambda resolver and streams the data to a queued service where the event/data is going to be retrieved by other services which are meant to handle it.

**SQS** - events queue, where the events are stored until some service is picking it up to handle it. Queue is needed no to lose the events which are coming from SNS.

**Store Lambda** - contains the business logic which is going to link the private data submit by the user with the 18+ decision made previously. This way, the key from **loSecDecision** (2) table is going to be used to link with the private data. The private data is going to be stored within **hiSecUserData** (3) table.
Also, after performing all the checks this lambda send to the **Analytics SNS** (4) an event contains the proper set o data needed for Analytics in this case.

**loSecDecision** - database table where the decision id the user is 18+ is stored. It’s needed in this flow to retrieve the key associated with the decision, so it can be used as an external key for the private data, to link it together with the decision record from an external table.

**hiSecUserData** - is an encrypted table where the private data is going to be stored securely. For the security/privacy reasons this table is going to be encrypted using a KMS key. Only the users/services with the right set o policy (to be allowed to access this KMS key) are allowed to access and decrypt the data from this table.

**KMS** - the storage of the encryption key, which is used to encrypt securely the private data of the user within **hiSecUserData** table.

**SNS Analytics** - the service which is receiving an event with the data which should be used for analytics purpose. 

**SQS Analytics** - events queue for analytics events.

**Analytics Lambda** - lambda function which is triggered by the queue of events with analytics data stream. Lambda is saving the analytics data within Redshift service.

**Redshift** - analytics data storage. Place where all analytics data is located.

