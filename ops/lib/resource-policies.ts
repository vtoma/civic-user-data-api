export const secretResourcesPolicy = {
    "Version": "2012-10-17",
    "Id": "Policy9999999990001",
    "Statement": [
        {
            "Sid": "Deny access except NotPrincipal list",
            "Effect": "Deny",
            "NotPrincipal": {
                "AWS": [
                    "arn:aws:iam::123000004567:role/xxxx-eb-ec2-role",
                    "arn:aws:iam::123000004567:user/Marianoruiz",
                    "arn:aws:iam::123000004567:user/pepe2222"
                ]
            },
            "Action": "s3:*",
            "Resource": "arn:aws:s3:::bucket-name"
        }
    ]
}

export const secretResourcesPolicy1 = {
    "Version": "2012-10-17",
    "Id": "Policy9999999990001",
    "Statement": [
        {
            "Sid": "Deny access except NotPrincipal list",
            "Effect": "Deny",
            "Condition": {
                "ArnNotEquals": [

                ]
            },
            "NotPrincipal": {
                "AWS": [
                    "arn:aws:iam::123000004567:role/xxxx-eb-ec2-role",
                    "arn:aws:iam::123000004567:user/Marianoruiz",
                    "arn:aws:iam::123000004567:user/pepe2222"
                ]
            },
            "Action": "s3:*",
            "Resource": "arn:aws:s3:::bucket-name"
        }
    ]
}