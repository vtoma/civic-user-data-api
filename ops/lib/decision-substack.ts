import {Construct} from '@aws-cdk/core';
import {ServiceStackProps} from './stack-props';
import {Topic, TopicPolicy} from '@aws-cdk/aws-sns';
import {Queue} from '@aws-cdk/aws-sqs';
import {Table} from '@aws-cdk/aws-dynamodb';
import {Stream} from '@aws-cdk/aws-kinesis';
import {Function} from '@aws-cdk/aws-lambda';
import {LambdaIntegration, RestApi, MethodLoggingLevel} from "@aws-cdk/aws-apigateway";
import {SqsSubscription} from '@aws-cdk/aws-sns-subscriptions';
import {SqsEventSource} from '@aws-cdk/aws-lambda-event-sources';
import {GeneralStack} from "./general";
import {PolicyStatement, AnyPrincipal} from "@aws-cdk/aws-iam";



export class DecisionStack extends GeneralStack {
    protected stack: Construct;
    protected prefix: string;
    
    protected sns: Topic;
    protected decisionQueue: Queue;
    protected inputLambda: Function;
    protected decisionLambda: Function;
    protected decisionTable: Table;
    protected decisionKinesis: Stream;

    constructor(stack: Construct, props: ServiceStackProps) {
        super(stack, props, props.stackPrefix);
        this.stack = stack;
        this.prefix = props.stackPrefix;
        return this;
    }

    generate() {
        this.createDecisionSns(); // SNS Dec
        this.createDecisionTable(); // loSecDecision
        this.createInputLambda(); // Input lambda
        this.createApiGateway(); // API Gateway
        this.createDecisionSqs(); // SQS Dec
        this.createDecisionLambda(); // Decision lambda
        this.storeAnalyticsLambda(); // Analytics lambda
    }

    protected createDecisionSns() {
        this.sns = new Topic(this.stack, `${this.prefix}ApiGatewayTopic`, {
            topicName: `${this.prefix}ApiGatewayTopic`
        });

        const topicPolicy = new TopicPolicy(this.stack, `${this.prefix}TopicPolicy`, {
            topics: [this.sns],
        });

        topicPolicy.document.addStatements(new PolicyStatement({
            actions: ["sns:Subscribe"],
            principals: [new AnyPrincipal()],
            resources: [this.sns.topicArn],
            conditions: {
                
            }
        }));
    }

    protected createInputLambda() {
        this.inputLambda = this.createLambda(`${this.prefix}InputLambda`, '../../src/lambda/input/lambda/input', undefined, {
            SNS_ARN: this.sns.topicArn,
        });

        this.sns.grantPublish(this.inputLambda);
        this.decisionTable.grantReadData(this.inputLambda);
    }

    protected createDecisionLambda() {
        this.decisionLambda = this.createLambda(`${this.prefix}DecisionLambda`, '../../src/lambda/decision/lambda/decision', undefined, {});
        this.decisionLambda.addEventSource(new SqsEventSource(this.decisionQueue));
        this.decisionTable.grantWriteData(this.decisionLambda);
    }

    protected storeAnalyticsLambda() {
        this.decisionLambda = this.createLambda(`${this.prefix}AnalyticsLambda`, '../../src/lambda/decision/lambda/decision', undefined, {});
        this.decisionLambda.addEventSource(new SqsEventSource(this.decisionQueue));
        this.decisionTable.grantWriteData(this.decisionLambda);
    }

    protected createApiGateway() {
        const apiId = "decision-api-" + this.prefix;
        const decisionApi = new RestApi(this.stack, apiId, {
            restApiName: apiId,
            description: "API decision.",
            deployOptions: {
                tracingEnabled: true,
                loggingLevel: MethodLoggingLevel.INFO
            },
            cloudWatchRole: true
        });

        const decisionPlanId = "decisionApiUsagePlan" + this.prefix;
        const decisionUsagePlan = decisionApi.addUsagePlan(decisionPlanId, {});

        const lambdaIntegration = new LambdaIntegration(this.inputLambda, {});

        const apiResource = decisionApi.root.addResource("step-1")

        apiResource.addMethod("POST", lambdaIntegration, {apiKeyRequired: false});

        decisionUsagePlan.addApiStage({stage: decisionApi.deploymentStage});
    }

    protected createDecisionSqs() {
        const decisionDLQ = new Queue(this.stack, `${this.prefix}DecisionDLQ`);
        this.decisionQueue = new Queue(this.stack, `${this.prefix}DecisionQueue`, {
            queueName: `${this.prefix}DecisionQueue`,
            deadLetterQueue: {
                queue: decisionDLQ,
                maxReceiveCount: 3
            }
        });

        this.sns.addSubscription(new SqsSubscription(this.decisionQueue));
    }

    protected createDecisionTable() {
        this.decisionKinesis = this.createKinesisStream("loSecDecisionStream");
        this.decisionTable = this.createDynamoTable("loSecDecision", this.decisionKinesis);
    }
}