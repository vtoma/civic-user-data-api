#!/bin/bash
set -e
echo Building...

GOOS=linux go build -o ../../../../../../ops/cdk.out/asset.93b1f50993c24b35102f08e327038a8f72f7da0a5929ac97daf14fd659b18a55/main ../

echo Running...
sam local invoke --profile payingit --env-vars ./env-vars.test.json --event ./event.test.json --template ../../../../../../ops/cdk.out/stackDevelop.template.json developcreateUserLambdaE7D33B7F
