package main

import (
	"context"
	"encoding/json"
	"os"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	log "github.com/sirupsen/logrus"
)

var (
	sess *session.Session
)

type sqsEvent struct {
	Records []sqsMessage `json:"Records"`
}

type sqsMessage struct {
	Message interface{}
}

func (m *sqsMessage) UnmarshalJSON(data []byte) error {
	var raw struct {
		Body string `json:"body"`
	}

	err := json.Unmarshal(data, &raw)
	if err != nil {
		log.WithError(err).Error("Error unmarshalling SNS message:", err.Error())
		return err
	}

	var rawMessage struct {
		Msg string `json:"Message"`
	}

	err = json.Unmarshal([]byte(raw.Body), &rawMessage)

	if err != nil {
		log.WithError(err).Error("Error unmarshalling SQS message:", err.Error())
		return err
	}

	err = json.Unmarshal([]byte(rawMessage.Msg), &m.Message)

	if err != nil {
		log.WithError(err).Error("Error unmarshalling final message:", err.Error())
		return err
	}

	return nil
}

func handleRequest(ctx context.Context, event sqsEvent) {
	log.WithField("Event:", event).Info("handleRequest invoked")
	for _, record := range event.Records {
		log.Info("SQS msg: ", record)
	}

}

func init() {
	if logLevel, err := log.ParseLevel(os.Getenv("LOG_LEVEL")); err == nil {
		log.SetLevel(logLevel)
	}
}

func main() {
	lambda.Start(handleRequest)
}
